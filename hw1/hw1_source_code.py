# -*- coding: utf-8 -*-
"""
CS571 Probablity Machine Learning HW1
Siyang Chen
"""
import os
import numpy as np
import pandas as pd
from keras.datasets import fashion_mnist
import matplotlib.pyplot as plt


###############################################################################
###############################################################################
###############################################################################

##**Preprocessing**##

(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

##input x_train
if os.path.exists('df_xTrain'):
    df_xTrain = pd.read_pickle('df_xTrain')
else:
    my_2darray = np.array([np.reshape(x, 784) for x in x_train])
    df_xTrain = pd.DataFrame(my_2darray)
    df_xTrain = df_xTrain.div(df_xTrain.sum(axis=1), axis=0)
    #df_xTrain[785] = df_xTrain[0]**5
    #df_xTrain[784] = 1
    df_xTrain = df_xTrain.sample(frac=1).reset_index(drop=True)
    df_xTrain.to_pickle('df_xTrain')

##output y_train
if os.path.exists('df_yTrain'):
    df_yTrain = pd.read_pickle('df_yTrain')
else:
    df_yTrain = pd.DataFrame(y_train, columns = ['fact_result'])
    df_yTrain.replace([0,1,2,3,4,5,6,7,8,9], [1,1,1,1,-1,1,1,1,1,-1],inplace = True)
    df_yTrain = df_yTrain.sample(frac=1).reset_index(drop=True)
    df_yTrain.to_pickle('df_yTrain')

##input x_test
if os.path.exists('df_xTest'):
    df_xTest = pd.read_pickle('df_xTest')
else:
    my_2darrayt = np.array([np.reshape(x, 784) for x in x_test])
    df_xTest = pd.DataFrame(my_2darrayt)
    df_xTest = df_xTest.div(df_xTest.sum(axis=1), axis=0)
    #df_xTest[785] = df_xTest[0]**5
    #df_xTest[784] = 1
    df_xTest = df_xTest.sample(frac=1).reset_index(drop=True)
    df_xTest.to_pickle('df_xTest')

##output y_test
if os.path.exists('df_yTest'):
    df_yTest = pd.read_pickle('df_yTest')
else:
    df_yTest = pd.DataFrame(y_test, columns = ['fact_result'])
    df_yTest.replace([0,1,2,3,4,5,6,7,8,9], [1,1,1,1,-1,1,1,1,1,-1],inplace = True)
    df_yTest = df_yTest.sample(frac=1).reset_index(drop=True)
    df_yTest.to_pickle('df_yTest')
    

###############################################################################
###############################################################################
###############################################################################

##**helper function**##
"""
  @param: dfx - dataframe of x_train, dfy- dataframe of y_train
  @return: weights of only considering the one of third dataset proceed
"""
def quesion1d_gain_w1of3(dfx, dfy):
    size = len(dfx.iloc[0])
    w_1of3 = np.zeros(size).tolist()
    #for recording the w_1of3 for question 1(d)
    df_x13 = dfx.iloc[0:int(len(dfx) / 3)]
    df_y13 = dfy.iloc[0:int(len(dfy) / 3)]

        
    #update the weights
    length = len(df_x13)
    for idx in range(length):
        tmp = (df_x13.iloc[idx] * w_1of3).sum()
        tmp = tmp * df_y13.iloc[idx]['fact_result']
        if tmp <= 0:
            w_tmp = np.add(w_1of3, (df_x13.iloc[idx] * df_y13.iloc[idx]['fact_result']).tolist())
            w_1of3 = w_tmp.tolist()
    
    return w_1of3


"""
  @param: w- weights, dfx - dataframe of x_train, dfy- dataframe of y_train, precision for ploting ROC
  @return: list of pair(TRP, FRP)
"""
def drawMyROC(w, dfx, dfy, precision = 100):
    # (1) Find the potential range of threshold
    dfa = dfx
    dfa = dfa * w;
    dfa['f(x)'] = dfa.sum(axis = 1)
    minVal = dfa['f(x)'].min()
    maxVal = dfa['f(x)'].max()
    sample = np.linspace(minVal, maxVal, precision)
    TRP = []
    FRP = []
    # (2) Use different threshold to compute the pair(TRP, FRP) for drawing the graph
    for b in reversed(sample):
        df_i = dfa
        df_i['f(x)-b'] = dfa['f(x)'] - b
        df_i['fact'] = dfy['fact_result']
        TP = len(df_i.loc[ (df_i['f(x)-b'] > 0) & (df_i['fact'] == 1)])
        FP = len(df_i.loc[ (df_i['f(x)-b'] > 0) & (df_i['fact'] == -1)])
        TN = len(df_i.loc[ (df_i['f(x)-b'] < 0) & (df_i['fact'] == -1)])
        FN = len(df_i.loc[ (df_i['f(x)-b'] < 0) & (df_i['fact'] == 1)])
        TRP.append(TP/(TP+FN))
        FRP.append(FP/(FP+TN))
    
    return [TRP, FRP]


"""
  @param: listY- y-axis values listX- x-axis values
  @return: AUC value
"""
def drawArea(listY, listX):
    #The pair is sorted by x value, and store seperate in ListY and ListX
    areaSum = 0
    for i in range(1, len(listX)):
        areaSum += (listX[i] - listX[i-1]) * listY[i-1]
        
    return areaSum

###############################################################################
###############################################################################
###############################################################################

##**Core function**##
"""
  @param: df_x - dataframe of x_train, df_y- dataframe of y_train, df_x_test - dataframe of x_test, df_y_test- dataframe of y_test,
          epoch- iteration times
  @return: void
"""
def perceptron(df_x, df_y, df_x_test, df_y_test, epoch):
    size = len(df_x.iloc[0])
    #weights initialization
    w = np.zeros(size).tolist()
    graph_accuracy_train = []
    graph_accuracy_test = []
    
    for step in range(epoch):
        print("step : ",step)
        
        #Testing Performance
        #on train_set
        df_train_weight = df_x * w;
        df_train_weight['yf(x)'] = df_train_weight.sum(axis = 1)
        df_train_weight['yf(x)'] = df_train_weight['yf(x)'] * df_y['fact_result']
        num_fail_train = len(df_train_weight.loc[df_train_weight['yf(x)']<=0])
        accuracy00 = (len(df_x) - num_fail_train) / len(df_x)
        graph_accuracy_train.append(accuracy00)
        print("graph_accuracy_train : ",accuracy00)

        
        #on test_set
        df_test_weight = df_x_test * w;
        df_test_weight['yf(x)'] = df_test_weight.sum(axis = 1)
        df_test_weight['yf(x)'] = df_test_weight['yf(x)'] * df_y_test['fact_result']
        num_fail_test = len(df_test_weight.loc[df_test_weight['yf(x)']<=0])
        accuracy01 = (len(df_x_test) - num_fail_test) / len(df_x_test)
        graph_accuracy_test.append(accuracy01)
        print("graph_accuracy_test : ",accuracy01)
        
        
        #training 
        length = len(df_x)
        for idx in range(length):
            tmp = (df_x.iloc[idx] * w).sum()
            tmp = tmp * df_y.iloc[idx]['fact_result']
            if tmp <= 0:
                w_tmp = np.add(w, (df_x.iloc[idx] * df_y.iloc[idx]['fact_result']).tolist())
                w = w_tmp.tolist()
        
        
    
    #For question 1(a) and 1(b)
    #plot graph of accuracy vs epoch
    plt.figure()
    x_axis = list(range(epoch - 0))
    label = ["accuracy_train", "accuracy_test"]  
    plt.plot(x_axis, graph_accuracy_train, color = 'red', linewidth = 0.85, linestyle = ':')
    plt.plot(x_axis, graph_accuracy_test, color = 'blue', linewidth = 0.85, linestyle = ':')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.title('Perceptronv Algorithm')
    plt.legend(label)
    plt.savefig('save00.png')
    
    #For question 1(c) calculating the confusion matrix
    df_last_test = df_x_test * w;
    df_last_test['f(x)'] = df_last_test.sum(axis = 1)
    df_last_test['fact'] = df_y_test['fact_result']
    TP = len(df_last_test.loc[ (df_last_test['f(x)'] > 0) & (df_last_test['fact'] == 1)])
    TN = len(df_last_test.loc[ (df_last_test['f(x)'] < 0) & (df_last_test['fact'] == -1)])
    FP = len(df_last_test.loc[ (df_last_test['f(x)'] > 0) & (df_last_test['fact'] == -1)])
    FN = len(df_last_test.loc[ (df_last_test['f(x)'] < 0) & (df_last_test['fact'] == 1)])
    print("For question 1(c) :")
    print("TP : %s, TN : %s, FP : %s, FN : %s" %(TP, TN, FP, FN))
    print("Accuracy: ", (TP + TN) / (TP + TN + FP + FN))
    
    #For question 1(d) drawing roc by two weight vectors
    w_1of3 = quesion1d_gain_w1of3(df_x, df_y)
    w_convergence = w
    res_w_1of3 = drawMyROC(w_1of3, df_x, df_y)
    res_w_convergence = drawMyROC(w_convergence, df_x, df_y)
    graph_w_1of3_y = res_w_1of3[0]
    graph_w_1of3_x = res_w_1of3[1]
    graph_w_convergence_y = res_w_convergence[0]
    graph_w_convergence_x = res_w_convergence[1]
    plt.figure()
    label = ['w_1of3', 'w_convergence']
    plt.plot(graph_w_1of3_x, graph_w_1of3_y, 'g-.')
    plt.plot(graph_w_convergence_x, graph_w_convergence_y, 'b-.')
    plt.xlabel('FRP')
    plt.ylabel('TRP')
    plt.legend(label)
    plt.title('ROC')
    plt.savefig('save01.png')
    plt.show()
    
    #For question 1(e)
    #Getting the value of AUC
    AUC_w_1of3 = np.trapz(graph_w_1of3_y, graph_w_1of3_x) #or use drawArea(graph_w_1of3_y, graph_w_1of3_x)
    AUC_w_convergence = np.trapz(graph_w_convergence_y, graph_w_convergence_x) #or use drawArea(graph_w_convergence_y, graph_w_convergence_x)
    print('AUC_w_1of3 : ', AUC_w_1of3)
    print('AUC_w_convergence : ', AUC_w_convergence)
    
    return


"""
  @param: df_x - dataframe of x_train, df_y- dataframe of y_train, df_x_test - dataframe of x_test, df_y_test- dataframe of y_test,
          beta - self identified parameter, epoch- iteration times
  @return: void
"""
def Balanced_Winnow(df_x, df_y, df_x_test, df_y_test, beta, epoch):
    p = len(df_x.iloc[0])
    wp = np.ones(p)
    wp = wp * (1 / (2 * p))
    wn = np.ones(p)
    wn = wn * (1 / (2 * p))
    graph_accuracy_train = []
    graph_accuracy_test = []
    for step in range(epoch):
        print("step : ",step)
        #Training
        length = len(df_x)
        for idx in range(length):
            tmp = (df_x.iloc[idx] * wp).sum() - (df_x.iloc[idx] * wn).sum()
            tmp = tmp * df_y.iloc[idx]['fact_result']
            if tmp <= 0:
                wp = wp * pow(np.e, beta * df_x.iloc[idx] * df_y.iloc[idx]['fact_result'])
                wn = wn * pow(np.e, beta * df_x.iloc[idx] * df_y.iloc[idx]['fact_result'] * (-1))
                s = wp.sum() + wn.sum()
                wp /= s
                wn /= s
        
        #Testing Performance
        #for train_set
        df_train_weight = df_x
        df_train_weight['yf(x)'] = (df_train_weight * wp).sum(axis = 1) - (df_train_weight * wn).sum(axis = 1)
        df_train_weight['yf(x)'] = df_train_weight['yf(x)'] * df_y['fact_result']
        num_fail_train = len(df_train_weight.loc[df_train_weight['yf(x)']<=0])
        accuracy00 = (len(df_x) - num_fail_train) / len(df_x)
        graph_accuracy_train.append(accuracy00)
        print("graph_accuracy_train : ",accuracy00)

        
        #for test_set
        df_test_weight = df_x_test
        df_test_weight['yf(x)'] = (df_test_weight * wp).sum(axis = 1) - (df_test_weight * wn).sum(axis = 1)
        df_test_weight['yf(x)'] = df_test_weight['yf(x)'] * df_y_test['fact_result']
        num_fail_test = len(df_test_weight.loc[df_test_weight['yf(x)']<=0])
        accuracy01 = (len(df_x_test) - num_fail_test) / len(df_x_test)
        graph_accuracy_test.append(accuracy01)
        print("graph_accuracy_test : ",accuracy01)
    
    
    #plot graph of accuracy vs epoch
    plt.figure()
    x_axis = list(range(epoch - 0))
    label = ["accuracy_train", "accuracy_test"]  
    plt.plot(x_axis, graph_accuracy_train, color = 'red', linewidth = 0.85, linestyle = ':')
    plt.plot(x_axis, graph_accuracy_test, color = 'blue', linewidth = 0.85, linestyle = ':')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.title('Winnow Algorithm')
    plt.legend(label)
    plt.savefig('save10.png')
    plt.show()
        
    #For question 2(a) calculating the confusion matrix
    df_last_test = df_x_test
    df_last_test['f(x)'] = (df_last_test * wp).sum(axis = 1) - (df_last_test * wn).sum(axis = 1)
    df_last_test['fact'] = df_y_test['fact_result']
    TP = len(df_last_test.loc[ (df_last_test['f(x)'] > 0) & (df_last_test['fact'] == 1)])
    TN = len(df_last_test.loc[ (df_last_test['f(x)'] < 0) & (df_last_test['fact'] == -1)])
    FP = len(df_last_test.loc[ (df_last_test['f(x)'] > 0) & (df_last_test['fact'] == -1)])
    FN = len(df_last_test.loc[ (df_last_test['f(x)'] < 0) & (df_last_test['fact'] == 1)])
    print("For question 2(a) :")
    print("TP : %s, TN : %s, FP : %s, FN : %s" %(TP, TN, FP, FN))
    print("Accuracy: ", (TP + TN) / (TP + TN + FP + FN))
    
    return

###############################################################################
###############################################################################
###############################################################################

##**Testing Function Area**##
    
perceptron(df_xTrain, df_yTrain, df_xTest, df_yTest, 100)
Balanced_Winnow(df_xTrain, df_yTrain, df_xTest, df_yTest, 0.2, 30)
