# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 22:51:58 2018

@author: siyang
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

###For Question 1
list_coordinates = [[0.75, 0.10], [0.85, 0.80], [0.85, 0.95], [0.15, 0.10], [0.05, 0.25], [0.05, 0.50], [0.85, 0.25]]
df_x = pd.DataFrame(list_coordinates, columns = ['x1', 'x2'])
print(df_x)

list_results = [-1, -1, 1, -1, 1, 1, -1]
df_y = pd.DataFrame(list_results, columns = ['y'])
print(df_y)


def perceptron(df_x, df_y, epoch):
    size = len(df_x.iloc[0])
    #weights initialization
    w = np.zeros(size).tolist()
    graph_accuracy_train = []
    
    for step in range(epoch):
        print("step : ",step)
        #training 
        length = len(df_x)
        for idx in range(length):
            tmp = (df_x.iloc[idx] * w).sum()
            tmp = tmp * df_y.iloc[idx]['y']
            if tmp <= 0:
                w_tmp = np.add(w, (df_x.iloc[idx] * df_y.iloc[idx]['y']).tolist())
                w = w_tmp.tolist()
        
        
        #Testing Performance
        #on train_set
        df_train_weight = df_x * w;
        df_train_weight['yf(x)'] = df_train_weight.sum(axis = 1)
        df_train_weight['yf(x)'] = df_train_weight['yf(x)'] * df_y['y']
        num_fail_train = len(df_train_weight.loc[df_train_weight['yf(x)']<=0])
        accuracy00 = (len(df_x) - num_fail_train) / len(df_x)
        graph_accuracy_train.append(accuracy00)
        print("graph_accuracy_train : ",accuracy00)

    #For question 1(a) and 1(b)
    #plot graph of accuracy vs epoch
    
    plt.figure()
    x_axis = np.arange(epoch)
    label = ["accuracy_train"]  
    plt.plot(x_axis, graph_accuracy_train, color = 'red', linewidth = 0.85, linestyle = ':')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.title('Perceptronv Algorithm')
    plt.legend(label)
    plt.show()
    
    return w


def drawTheClassifier(weights):
    #label = ["Three Points Shot", "Two Points Shot"] 
    #Plot the Postive
    plt.plot([0.85, 0.05, 0.05], [0.95, 0.25, 0.50], 'r+')
    #Plot the Negative
    plt.plot([0.75, 0.85, 0.15, 0.85], [0.10, 0.80, 0.10, 0.25], 'bo')
    #Plot the Classifier
    a = weights[0]
    b = weights[1]
    plt.plot([0, 1], [0, 1 * (b / a) * (-1)])
    plt.plot([0, 1], [0, 1 * (0.95 / 0.85)], 'y-')
    plt.plot([0, 1], [0, 1 * (0.80 / 0.85)], 'y-')
    #Other Artributes
    plt.axis([0, 1, 0, 1])
    plt.xlabel('x1')
    plt.ylabel('x2')
    #plt.legend(label)
    plt.show()

#weights = perceptron(df_x, df_y, 100)
#x_axis = np.arange(100)
#print(x_axis)
drawTheClassifier(perceptron(df_x, df_y, 5))
