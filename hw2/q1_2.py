# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 23:45:23 2018

@author: siyan
"""
import pandas as pd
import matplotlib.pyplot as plt

tmpList = [[0.05, 0.25, 1], 
           [0.05, 0.50, 1], 
           [0.15, 0.10, -1],
           [0.75, 0.10, -1],
           [0.85, 0.25, -1], 
           [0.85, 0.80, -1],
           [0.85, 0.95, 1]]
df_x = pd.DataFrame(tmpList, columns = ['x1', 'x2', 'y'])
print(df_x)

def GINI(prob):
    return 2 * prob * (1 - prob)

def buildTree_2d(df_x): #Assume Only Two Features
    N = len(df_x) #nums of sample

    #<1> Choose the threshold for 'x1'
    gainSave = 0
    good_threshold = df_x.loc[0]['x1']
    for idx in range(1, N):
        threshold = (df_x.loc[idx]['x1'] + df_x.loc[idx-1]['x1']) / 2
        print(threshold)
        N0 = len(df_x.loc[df_x['x1'] < threshold])
        N1 = len(df_x.loc[df_x['x1'] >= threshold])
        print(N0)
        print(N1)
        P_count = len(df_x.loc[df_x['y'] == 1])
        P0_count = len(df_x.loc[ (df_x['x1'] < threshold) & (df_x['y'] == 1) ])
        P1_count = len(df_x.loc[ (df_x['x1'] >= threshold) & (df_x['y'] == 1) ])
        print(P0_count)
        print(P1_count)
        print("************")
        gini_p = GINI(P_count / N)
        gini_p0 = 0
        gini_p1 = 0
        if N0 != 0:
            gini_p0 = GINI(P0_count / N0)
        if N1 != 0:
            gini_p1 = GINI(P1_count / N1)
        
        if gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1 > gainSave:
            gainSave = gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1
            good_threshold = threshold

    print("^^^^^^^^^^^^^^")
    print(good_threshold)
    print("^^^^^^^^^^^^^^")

    
    #<2> Choose the threshold for 'x2'
    df_x2 = df_x.loc[df_x['x1'] >= good_threshold]
    del df_x2['x1']
    df_x2 = df_x2.sort_values(by=['x2'])
    df_x2.reset_index(drop = True, inplace = True)
    print(df_x2)
    
    gainSave = 0
    good_threshold2 = df_x2.loc[0]['x2']
    for idx in range(1, len(df_x2)):
        threshold = (df_x2.loc[idx]['x2'] + df_x2.loc[idx-1]['x2']) / 2
        print(threshold)
        N0 = len(df_x2.loc[df_x2['x2'] < threshold])
        N1 = len(df_x2.loc[df_x2['x2'] >= threshold])
        print(N0)
        print(N1)
        P_count = len(df_x2.loc[df_x2['y'] == 1])
        P0_count = len(df_x2.loc[ (df_x2['x2'] < threshold) & (df_x2['y'] == 1) ])
        P1_count = len(df_x2.loc[ (df_x2['x2'] >= threshold) & (df_x2['y'] == 1) ])
        print(P0_count)
        print(P1_count)
        print("************")
        gini_p = GINI(P_count / N)
        gini_p0 = 0
        gini_p1 = 0
        if N0 != 0:
            gini_p0 = GINI(P0_count / N0)
        if N1 != 0:
            gini_p1 = GINI(P1_count / N1)
        
        if gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1 > gainSave:
            gainSave = gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1
            good_threshold2 = threshold
    
    print("^^^^^^^^^^^^^^")
    print(good_threshold2)
    print("^^^^^^^^^^^^^^")
    
    
    #<3> Plot Graph
    label = ["Three Points Shot", "Two Points Shot", "x1 split: %s" %good_threshold, "x2 split: %s" %good_threshold2] 
    #Plot the Postive
    plt.plot([0.85, 0.05, 0.05], [0.95, 0.25, 0.50], 'r+')
    #Plot the Negative
    plt.plot([0.75, 0.85, 0.15, 0.85], [0.10, 0.80, 0.10, 0.25], 'bo')
    
    #Plot x1 Binary Classifier
    plt.plot([good_threshold, good_threshold], [0, 1])
    #Plot x2 Binary Classifier
    plt.plot([good_threshold, 1], [good_threshold2, good_threshold2])
    

    #plt.plot([0, 1], [0.9, 0.9], 'g-')
    plt.plot([0.08, 0.08], [0, 1], 'g-')
    plt.plot([0.08, 1], [0.9, 0.9], 'g-')
    """
    label = ["Three Points Shot", "Two Points Shot", "x2 split: %s" %0.9, "x1 split: %s" %0.08] 
    """
    
    plt.axis([0, 1, 0, 1])
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.legend(label)
    plt.title('Decision Tree on Observed Shots')
    plt.show()
    
    
buildTree_2d(df_x)