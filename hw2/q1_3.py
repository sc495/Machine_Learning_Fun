#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  3 16:26:30 2018

@author: siyangchen
"""

import numpy as np
import matplotlib.pyplot as plt
import math

list_x = np.array([i for i in range(0, 100)]) / 100
list_y = [math.sqrt(num) for num in list_x]

#label = ["Three Points Shot", "Two Points Shot"]
#plt.plot([0.85, 0.05, 0.05], [0.95, 0.25, 0.50], 'r+')
#Plot the Negative
#plt.plot([0.75, 0.85, 0.15, 0.85], [0.10, 0.80, 0.10, 0.25], 'bo')

k = math.sqrt(2)
s = 1/3/pow(k, 3) - 1/2/k + 1/3
print("Loss: %s" %s)

#plt.plot([0,1], [0,1])
plt.plot(list_x, list_y)
#Other Artributes

plt.plot([0, 1], [0, 1 * ((1/k) / (1/k**2))])
plt.plot([0, 1], [0, 1 * (0.95 / 0.85)], 'y-')
plt.plot([0, 1], [0, 1 * (0.80 / 0.85)], 'y-')


plt.axis([0, 1, 0, 1])
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Three Points Curve')
#plt.legend(label)
plt.show()

