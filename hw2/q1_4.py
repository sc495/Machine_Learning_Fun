#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  3 21:17:32 2018

@author: siyangchen
"""

from sympy import Symbol, solve, diff
import numpy as np
import math
import matplotlib.pyplot as plt

x = Symbol('x')

#2/3*pow(1/2*x, 3/2) - x*pow(1/2*x, 1/2) + 2/3*pow(x,3/2) + 2/3*pow((x+1)/2,3/2) - (x+1)*pow((x+1)/2,1/2) + 2/3*pow(x,3/2) + 2/3

s1 = solve(diff(2/3*math.sqrt(1/8)*pow(x,3/2) - x*math.sqrt(1/2)*pow(x,1/2) + 2/3*pow(x,3/2) + 2/3*math.sqrt(1/8)*pow(x+1, 3/2) - (x+1)*math.sqrt(1/2)*pow(x+1, 1/2) + 2/3*pow(x, 3/2) + 2/3,x), x)
s1 = s1[0]
s2 = math.sqrt(1 / 2 * s1)
s3 = math.sqrt((s1 + 1) / 2)
error = 2/3*math.sqrt(1/8)*pow(s1,3/2) - s1*math.sqrt(1/2)*pow(s1,1/2) + 2/3*pow(s1,3/2) + 2/3*math.sqrt(1/8)*pow(s1+1, 3/2) - (s1+1)*math.sqrt(1/2)*pow(s1+1, 1/2) + 2/3*pow(s1, 3/2) + 2/3
print("***********")
print("First Decision Tree")
print("s1: %s" %s1)
print("s2: %s" %s2)
print("s3: %s" %s3)
print("error: %s" %error)


"""
label = ["Three Points Shot", "s1: %s" %s1, "s2: %s" %s2, "s3: %s" %s3]
list_x = np.array([i for i in range(0, 100)]) / 100
list_y = [math.sqrt(num) for num in list_x]
plt.plot(list_x, list_y)
plt.plot([s1, s1],[0, 1])
plt.plot([0, s1],[s2, s2])
plt.plot([s1, 1],[s3, s3])

#Plot the Postive
plt.plot([0.85, 0.05, 0.05], [0.95, 0.25, 0.50], 'r+')
#Plot the Negative
plt.plot([0.75, 0.85, 0.15, 0.85], [0.10, 0.80, 0.10, 0.25], 'bo')
plt.plot([0.1, 0.1], [0, 1], 'y-.')
plt.plot([0.1, 1], [0.875, 0.875], 'y-.')

plt.axis([0, 1, 0, 1])
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Three Points Curve')
plt.legend(label)
plt.show()
"""

###################################################
###################################################
###################################################

x = Symbol('x')

ans = solve(diff(7/12*pow(x,3) + 4/3*pow((x+1)/2,3) - (x+1)*pow((x+1)/2,2) + 1/3,x), x)
s1 = ans[1]
s2 = 1/4*pow(s1,2)
s3 = pow((s1+1)/2,2)
error = 7/12*pow(s1,3) + 4/3*pow((s1+1)/2,3) - (s1+1)*pow((s1+1)/2,2) + 1/3
print("***********")
print("Second Decision Tree")
print("s1: %s" %s1)
print("s2: %s" %s2)
print("s3: %s" %s3)
print("error: %s" %error)


label = ["Three Points Shot", "s1: %s" %s1, "s2: %s" %s2, "s3: %s" %s3]
list_x = np.array([i for i in range(0, 100)]) / 100
list_y = [math.sqrt(num) for num in list_x]
plt.plot(list_x, list_y)
plt.plot([0, 1],[s1, s1])
plt.plot([s2, s2],[0, s1])
plt.plot([s3, s3],[s1, 1])


plt.plot([0.85, 0.05, 0.05], [0.95, 0.25, 0.50], 'r+')
plt.plot([0.75, 0.85, 0.15, 0.85], [0.10, 0.80, 0.10, 0.25], 'bo')
plt.plot([0.1, 0.1], [0, 1], 'y-.')
plt.plot([0.1, 1], [0.875, 0.875], 'y-.')

plt.axis([0, 1, 0, 1])
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Three Points Curve')
plt.legend(label)
plt.show()

