#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 19:14:59 2018

@author: siyangchen
"""
from sympy import Symbol, solve, diff
import matplotlib.pyplot as plt


#label = ['case1', 'case2']


#plt.plot([0, 1], [0, 0.6])
plt.plot([0, 1], [0, 0.3])
#plt.legend(label)

plt.plot([0.50, 0.50], [0, 0.25], 'b-')
plt.plot([0.50, 1], [0.25, 0.25], 'b-')
plt.plot([1, 1], [0, 0.25], 'b-')


plt.axis([0, 1, 0, 1])
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Basketball Court')

plt.show()

###################################################
##################################################
##################################################
print("***********")
k = Symbol('k')
ans = solve(diff(3/4*pow(k, 1) + 1/16*pow(k,-1) - 3/8, k),k)[1]
error = 3/4*pow(ans, 1) + 1/16*pow(ans,-1) - 3/8
print("k: %s" %ans)
print("error: %s" %error)