#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 19:33:45 2018

@author: siyangchen
"""

import matplotlib.pyplot as plt


label = ["x1:0.5", "x2:0.25"]
plt.plot([0.5, 0.5], [0, 1])
plt.plot([0.5, 1],[0.25, 0.25])
plt.legend(label)


plt.plot([0.50, 0.50], [0, 0.25], 'g-.')
plt.plot([0.50, 1], [0.25, 0.25], 'g-.')
plt.plot([1, 1], [0, 0.25], 'g-.')


plt.axis([0, 1, 0, 1])
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Basketball Court')

plt.show()
