#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  4 21:22:47 2018

@author: siyangchen
"""

import pandas as pd

df_train = pd.read_csv('train.csv')
#print(df_train)
df_test = pd.read_csv('test.csv')
#print(df_train)


def GINI(prob):
    return 2 * prob * (1 - prob)


#Best Split
def for_2_1_Best_Split(df_train):
    impurity_values = []
    features_num = len(df_train.loc[0]) - 1
    N = len(df_train)
    #print(features_num)
    for i in range(0, features_num):
        column_name = "X%s" %(i+1)
        #print(column_name)
        N0 = len(df_train.loc[df_train[column_name] == 0])
        N1 = len(df_train.loc[df_train[column_name] == 1])
        P_count = len(df_train.loc[df_train['Y'] == 1])
        P0_count = len(df_train.loc[ (df_train[column_name] == 0) & (df_train['Y'] == 1) ])
        P1_count = len(df_train.loc[ (df_train[column_name] == 1) & (df_train['Y'] == 1) ])
        gini_p = GINI(P_count / N)
        gini_p0 = 0
        gini_p1 = 0
        if N0 != 0:
            gini_p0 = GINI(P0_count / N0)
        if N1 != 0:
            gini_p1 = GINI(P1_count / N1)
        tmp =  gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1
        impurity_values.append(tmp)
    
    print("Method1 --- Best Split")
    print(impurity_values)
    #print(impurity_values.index(max(impurity_values)))
    return impurity_values

#Best Surrogate Split
def for_2_1_Best_Surrogate_Split(df_train):
    
    impurity_values = for_2_1_Best_Split(df_train)
    
    impurity_surrogates_values = []
    features_num = len(df_train.loc[0]) - 1
    N = len(df_train)
    for i in range(0, features_num):
        curr = "X%s" %(i+1)
        tmp_part1 = impurity_values[i]
        
        lamdalist = []
        for j in range(0, features_num):
            if i == j:
                lamdalist.append(-1)
            else:
                candidate = "X%s" %(j+1)
                Prob_left = len(df_train.loc[df_train[candidate] == 0]) / N
                Prob_right = len(df_train.loc[df_train[candidate] == 1]) / N
                Prob_left_left =len(df_train.loc[ (df_train[curr] == 0) & (df_train[candidate] == 0) ]) / N
                Prob_right_right =len(df_train.loc[ (df_train[curr] == 1) & (df_train[candidate] == 1) ]) / N
                res = (min(Prob_left, Prob_right) - (1 - Prob_left_left - Prob_right_right)) / min(Prob_left, Prob_right)
                lamdalist.append(res)
        
        selected_idx = lamdalist.index(max(lamdalist))
        
        tmp_part2 = impurity_values[selected_idx]
        
        impurity_surrogates_values.append(tmp_part1 + tmp_part2)
    
    print("Method2 --- Best Surrogate Split")
    print(impurity_surrogates_values)
    #print(impurity_surrogates_values.index(max(impurity_surrogates_values)))
    return impurity_surrogates_values

#If We already knew which features we are going to use
def mean_least_square_error(df_train, df_test, feature_name):
    M = len(df_test)
    
    #see which binary num is corresponding to which label
    pred1 =len(df_train.loc[ (df_train[feature_name] == 0) & (df_train['Y'] == 0) ])
    pred2 =len(df_train.loc[ (df_train[feature_name] == 0) & (df_train['Y'] == 1) ])
    
    mean_least_square_error = 0
    if pred1 >= pred2:
        print("corresponding relationship: feature 0 --> label 0")
        print("corresponding relationship: feature 1 --> label 1")
        wrong1_count = len(df_test.loc[ (df_test[feature_name] == 0) & (df_test['Y'] == 1) ])
        wrong2_count = len(df_test.loc[ (df_test[feature_name] == 1) & (df_test['Y'] == 0) ])
        mean_least_square_error = (wrong1_count + wrong2_count) / M
    else:
        print("corresponding relationship: feature 0 --> label 1")
        print("corresponding relationship: feature 1 --> label 0")
        wrong1_count = len(df_test.loc[ (df_test[feature_name] == 0) & (df_test['Y'] == 0) ])
        wrong2_count = len(df_test.loc[ (df_test[feature_name] == 1) & (df_test['Y'] == 1) ])
        mean_least_square_error = (wrong1_count + wrong2_count) / M
        
    return mean_least_square_error

for_2_1_Best_Surrogate_Split(df_train)

print(mean_least_square_error(df_train, df_test, 'X1'))
print(mean_least_square_error(df_train, df_test, 'X2'))
