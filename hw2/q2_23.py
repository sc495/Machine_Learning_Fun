#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 14:30:53 2018

@author: siyangchen
"""
from copy import deepcopy
import pandas as pd
import numpy as np
import random


##########################3
##Please go to the bottom to test the code: )
##Just uncomment them by each question, and you will see the results: )

M = 1000

df_train = pd.read_csv('train.csv')
df_test = pd.read_csv('test.csv')



def GINI(prob):
    return 2 * prob * (1 - prob)

def Best_Split(df_train, seedLists):
    impurity_values = []
    N = len(df_train)
    #print(features_num)
    for i in seedLists:
        column_name = "X%s" %(i+1)
        #print(column_name)
        N0 = len(df_train.loc[df_train[column_name] == 0])
        N1 = len(df_train.loc[df_train[column_name] == 1])
        P_count = len(df_train.loc[df_train['Y'] == 1])
        P0_count = len(df_train.loc[ (df_train[column_name] == 0) & (df_train['Y'] == 1) ])
        P1_count = len(df_train.loc[ (df_train[column_name] == 1) & (df_train['Y'] == 1) ])
        gini_p = GINI(P_count / N)
        gini_p0 = 0
        gini_p1 = 0
        if N0 != 0:
            gini_p0 = GINI(P0_count / N0)
        if N1 != 0:
            gini_p1 = GINI(P1_count / N1)
        tmp =  gini_p - (N0 / N) * gini_p0 - (N1 / N) * gini_p1
        impurity_values.append(tmp)
    
    #print("Method1 --- Best Split")
    #print(impurity_values)
    #print(impurity_values.index(max(impurity_values)))
    return impurity_values


def Best_Surrogate_Split(df_train, impurity_values, seedLists):
    
    #impurity_values = for_2_1_Best_Split(df_train)
    
    impurity_surrogates_values = []
    #features_num = len(df_train.loc[0]) - 1
    N = len(df_train)
    k = 0
    for i in seedLists:
        curr = "X%s" %(i+1)
        tmp_part1 = impurity_values[k]
        
        lamdalist = []
        for j in seedLists:
            if i == j:
                lamdalist.append(-1)
            else:
                candidate = "X%s" %(j+1)
                Prob_left = len(df_train.loc[df_train[candidate] == 0]) / N
                Prob_right = len(df_train.loc[df_train[candidate] == 1]) / N
                Prob_left_left =len(df_train.loc[ (df_train[curr] == 0) & (df_train[candidate] == 0) ]) / N
                Prob_right_right =len(df_train.loc[ (df_train[curr] == 1) & (df_train[candidate] == 1) ]) / N
                res = (min(Prob_left, Prob_right) - (1 - Prob_left_left - Prob_right_right)) / min(Prob_left, Prob_right)
                lamdalist.append(res)
        
        selected_idx = lamdalist.index(max(lamdalist))
        
        tmp_part2 = impurity_values[selected_idx]
        
        impurity_surrogates_values.append(tmp_part1 + tmp_part2)
        k += 1
    
    #print("Method1 --- Best Surrogate Split")
    #print(impurity_surrogates_values)
    #print(impurity_surrogates_values.index(max(impurity_surrogates_values)))
    return impurity_surrogates_values



def shuffle_df(df_xy):  
    tmp = df_xy.drop('Y', axis = 1)
    tmp = tmp.sample(frac = 1)
    tmp.reset_index(inplace = True, drop = True)
    tmp['Y'] = df_xy['Y']
    return tmp



def calculate_oob_error_singleTree(df_bootstrap, df_oob, feature_name):
    M = len(df_oob)
    df_oob_perm = shuffle_df(df_oob)
    
    #see which binary num is corresponding to which label
    pred1 =len(df_bootstrap.loc[ (df_bootstrap[feature_name] == 0) & (df_bootstrap['Y'] == 0) ])
    pred2 =len(df_bootstrap.loc[ (df_bootstrap[feature_name] == 0) & (df_bootstrap['Y'] == 1) ])
    
    mean_least_square_error = 0
    if pred1 >= pred2:
        #print("corresponding relationship: feature 0 --> label 0")
        #print("corresponding relationship: feature 1 --> label 1")
        wrong1_perm = len(df_oob_perm.loc[ (df_oob_perm[feature_name] == 0) & (df_oob_perm['Y'] == 1) ])
        wrong2_perm = len(df_oob_perm.loc[ (df_oob_perm[feature_name] == 1) & (df_oob_perm['Y'] == 0) ])
        wrong1_oob = len(df_oob.loc[ (df_oob[feature_name] == 0) & (df_oob['Y'] == 1) ])
        wrong2_oob = len(df_oob.loc[ (df_oob[feature_name] == 1) & (df_oob['Y'] == 0) ])
        perm_mlse = (wrong1_perm + wrong2_perm) / M
        oob_mlse = (wrong1_oob + wrong2_oob) / M
        mean_least_square_error = perm_mlse - oob_mlse
    else:
        #print("corresponding relationship: feature 0 --> label 1")
        #print("corresponding relationship: feature 1 --> label 0")
        wrong1_perm = len(df_oob_perm.loc[ (df_oob_perm[feature_name] == 0) & (df_oob_perm['Y'] == 0) ])
        wrong2_perm = len(df_oob_perm.loc[ (df_oob_perm[feature_name] == 1) & (df_oob_perm['Y'] == 1) ])
        wrong1_oob = len(df_oob.loc[ (df_oob[feature_name] == 0) & (df_oob['Y'] == 0) ])
        wrong2_oob = len(df_oob.loc[ (df_oob[feature_name] == 1) & (df_oob['Y'] == 1) ])
        perm_mlse = (wrong1_perm + wrong2_perm) / M
        oob_mlse = (wrong1_oob + wrong2_oob) / M
        mean_least_square_error = perm_mlse - oob_mlse
        
    return mean_least_square_error



def buildForest(df_train, stumps, K, B):
    df_train_copy = df_train
    forestLists = [] #should be 100, and records the features as selected from 0, 1, 2 ,3
    
    #for <1> How many times is each variable the best split
    map_best_split_counts = [0, 0, 0, 0, 0]   #index means the label of X
    map_best_surrogate_splits = [0, 0, 0, 0, 0]  #index means the label of X
    #for <2> For knowing the impurity of the forest
    Imp_List = [0, 0, 0, 0, 0]
    Imp_OOB_List = [0, 0, 0, 0, 0]
    
    #(1) Build Each Single Tree
    for t in range(0, stumps):
        #random select features
        seeds_list = random.sample(range(0,5),K)
        
        remove_list = list(range(0,5))
        remove_list = [x for x in remove_list if x not in seeds_list]
        #get the potential candidate for this stump
        df_tree = deepcopy(df_train_copy) ##important df_tree
        for i in remove_list:
            curr_name = "X%s" %(i+1)
            del df_tree[curr_name]
        
        #get the bootstrap sample and out of bag sample
        df_bootstrap = df_tree.sample(B, replace = True) ##important df_bootstrap
        remove_index = df_bootstrap.index.tolist()
        remove_index = set(remove_index)
        remove_index = list(remove_index)
        df_bootstrap.reset_index(inplace = True, drop = True)
        df_oob = df_tree.drop(df_tree.index[remove_index]) ##important df_oob
        df_oob.reset_index(inplace = True, drop = True)
        #print(df_bootstrap)
        #print(df_oob)
        
        #select the feature with largest impurity_best_split value
        impurity_values_list = Best_Split(df_bootstrap, seeds_list)
        #print(impurity_values_list)
        selected_feature_best_split = seeds_list[impurity_values_list.index(max(impurity_values_list))] ##important
        #print(selected_feature_best_split)
        
        #select the feature with largest impurity_best_surrogate_split value
        impurity_surrogates_values_list = Best_Surrogate_Split(df_bootstrap, impurity_values_list, seeds_list)
        no_need_to_replace = impurity_values_list.index(max(impurity_values_list))
        impurity_surrogates_values_list[no_need_to_replace] = -1
        #print(impurity_surrogates_values_list)
        selected_feature_best_surrogate_split = seeds_list[impurity_surrogates_values_list.index(max(impurity_surrogates_values_list))]
        #print(selected_feature_best_surrogate_split)
        
        
        
        #Counts best split feature and best surrogate feature
        map_best_split_counts[selected_feature_best_split] += 1
        if selected_feature_best_split != selected_feature_best_surrogate_split:
            map_best_surrogate_splits[selected_feature_best_surrogate_split] += 1
        
        
        #Calculate two error list, which saves each stumps error
        #Impurity List
        j = 0
        for idx in seeds_list:
            Imp_List[idx] += impurity_values_list[j]
            j += 1
            
        
        #Impurity OOB List
        selected_feature = "X%s" %(selected_feature_best_split+1)
        mlseVal = calculate_oob_error_singleTree(df_bootstrap, df_oob, selected_feature)
        Imp_OOB_List[selected_feature_best_split] += mlseVal
        
        #Save the created trees to forest
        forestLists.append(selected_feature_best_split)
        
        
        
    print("***each variable***")
    print("best_split:")
    print(map_best_split_counts)
    print("map_best_surrogate_splits:")
    print(map_best_surrogate_splits)
    
    
    standard_deviation_onSample = np.std(Imp_List)
    standard_deviation_onSample_OOB = np.std(Imp_OOB_List)
    
    print("***Impurity List Recording Each Stump***")
    print("Imp_Value:")
    Imp_List = np.divide(Imp_List, stumps)
    print(Imp_List)
    #print(np.mean(Imp_List))
    print("Imp_OOB_Value:")
    Imp_OOB_List = np.divide(Imp_OOB_List, stumps)
    print(Imp_OOB_List)
    #print(np.mean(Imp_OOB_List))
    

    
    return [forestLists, Imp_List, Imp_OOB_List, standard_deviation_onSample, standard_deviation_onSample_OOB]


def FirstMethod(df_train, df_test, forestLists): #Majority Votes
    N = len(df_test)
    error = 0
    for idx in range(0, N):
        tmp_error = 0
        vote_1 = 0
        vote_0 = 0
        #Beginning of Democrotic
        for tree in forestLists:
            currName = "X%s" %(tree + 1)
            pred1 =len(df_train.loc[ (df_train[currName] == 0) & (df_train['Y'] == 0) ])
            pred2 =len(df_train.loc[ (df_train[currName] == 0) & (df_train['Y'] == 1) ])
            if pred1 > pred2:
                if df_test.loc[idx][currName] == 0:
                    vote_0 += 1
                else:
                    vote_1 += 1
            else:
                if df_test.loc[idx][currName] == 0:
                    vote_1 += 1
                else:
                    vote_0 += 1
        #See which Votes Win
        if vote_1 > vote_0:
            tmp_error = abs(1 - df_test.loc[idx]['Y'])
        else:
            tmp_error = abs(0 - df_test.loc[idx]['Y'])
        error += tmp_error
    return error / N


def SecondMethod(df_train, df_test, forestLists): #mlsl for each predictions and average it
    N = len(df_test)
    error = 0
    for idx in range(0, N):
        tmp_error = 0
        #Calculate Everyone's Loss
        for tree in forestLists:
            currName = "X%s" %(tree + 1)
            pred1 =len(df_train.loc[ (df_train[currName] == 0) & (df_train['Y'] == 0) ])
            pred2 =len(df_train.loc[ (df_train[currName] == 0) & (df_train['Y'] == 1) ])
            if pred1 > pred2:
                if df_test.loc[idx][currName] == 0:
                    tmp_error += abs(0 - df_test.loc[idx]['Y'])
                else:
                    tmp_error += abs(1 - df_test.loc[idx]['Y'])
            else:
                if df_test.loc[idx][currName] == 0:
                    tmp_error += abs(1 - df_test.loc[idx]['Y'])
                else:
                    tmp_error += abs(0 - df_test.loc[idx]['Y'])
        #MLSL on this sample
        tmp_error = tmp_error / len(df_test) #Key different to First Method
        error += tmp_error
    
    return error / len(forestLists)


def testingDifferentBoostrapSize(df_train, trumps, K, qList):
    n = len(df_train)
    BList = []
    for q in qList:
        B = int(q * n)
        BList.append(B)
        tmpTuple = buildForest(df_train, trumps, K, B)
        
        Imp_List = tmpTuple[1]
        Imp_OOB_List = tmpTuple[2]
        standard_deviation_onSample = tmpTuple[3]
        standard_deviation_onSample_OOB = tmpTuple[4]
        
        print("******************************************")
        print("For last question: Testing different B !!!")
        print("q:%s  B:%s" %(q, B))
        
        #For question1
        print("For question 2c (i)")
        print("Imp_List:")
        print(Imp_List)
        print("Imp_OOB_List:")
        print(Imp_OOB_List)
        
         #For question2 #instead of mean on each trump
        print("For question 2c (ii)")
        print("standard_deviation_onSample:")
        print(standard_deviation_onSample)
        print("standard_deviation_onSample_OOB:")
        print(standard_deviation_onSample_OOB)
        

    
    return

##################################################################
############
##For Question 2a
"""
for i in range(1, 6):
    forestLists = buildForest(df_train, 1000, i, 400)[0]
"""


##################################################################
############
##For Question 2b
"""

forestLists = buildForest(df_train, 1000, 5, 400)[0]

print("*******************************")
print("Calculate the MLSL on test data...")
print("FirstMethod:")
mlse_first_method = FirstMethod(df_train, df_test, forestLists)
print(mlse_first_method)
print("SecondMethod:")
mlse_second_method = SecondMethod(df_train, df_test, forestLists)
print(mlse_second_method)

"""

##################################################################
############
##For Question 2c

print("******************************")
print("Running qList = [0.4, 0.5, 0.6, 0.7, 0.8]...")
qList = [0.4, 0.5, 0.6, 0.7, 0.8]
testingDifferentBoostrapSize(df_train, 1000, 2, qList)

