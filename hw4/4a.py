#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 17:19:48 2018

@author: siyangchen
"""

import numpy as np
from cvxopt import matrix, solvers

def train(x, y):
    
    m = len(x)
    K = x * y[:,None]
    K = np.dot(K, K.T)
    
    Q = matrix(K)
    p = matrix(-np.ones((m, 1)))
    G = matrix(-np.eye(m))
    h = matrix(np.zeros(m))
    A = matrix(y.reshape(1, -1))
    b = matrix(np.zeros(1))
    solvers.options['show_progress'] = False
    sol = solvers.qp(Q, p, G, h, A, b)
    
    alphas = np.array(sol['x'])
    
    weights = np.sum(alphas * y[:, None] * x, axis = 0)
    bias = 0
    for idx in range(0, m):
        if alphas > 0:
            bias = y[idx] - np.dot(weights, x[idx])
    
    return [weights, bias]


def predict(x, y, weights, bias):
    res_vec = np.dot(x, weights) + bias
    return np.sign(res_vec)
