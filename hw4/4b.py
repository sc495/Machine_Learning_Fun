#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 17:56:54 2018

@author: siyangchen
"""

import copy

import pandas as pd
import numpy as np

from sklearn import svm, preprocessing
from sklearn.model_selection import train_test_split

from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt

np.random.seed(2018)

df_data = pd.read_csv('creditCard.csv')

df_x = copy.deepcopy(df_data)
df_x.drop(columns=['Class'], inplace = True)
df_y = df_data['Class']

X = df_x.as_matrix()
y = df_y.as_matrix()

#X = preprocessing.normalize(X, axis = 0)
X = preprocessing.scale(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.1, 
                                                    random_state = 2018)

print(len(X_train))
print(len(X_test))

classifier = svm.SVC(kernel='linear', probability=True)
y_score = classifier.fit(X_train, y_train).decision_function(X_test)
y_predict = classifier.predict(X_test)
accuracy = np.array(y_predict == y_test).sum() / len(y_test)

print(y_score)

fpr, tpr, thresholds = roc_curve(y_test, y_score)
roc_auc = auc(fpr, tpr)


plt.figure()
plt.plot(fpr, tpr, 'darkorange', label='AUC = %s, Accuracy = %s' %(roc_auc, accuracy))
plt.plot([0, 1], [0, 1], 'b--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()

