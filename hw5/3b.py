#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 18 16:48:28 2018

@author: siyangchen
"""

import pandas as pd
import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt

np.random.seed(0)
df = pd.read_csv('X.csv')
X = df.as_matrix()
X = preprocessing.scale(X)

print(X)

Beta1 = [0.1, 0.3, 0.2, 0.2, 0.9, 0.8, 0.9, 0.1, 0.4, 0.2, 0.7, 0.3, 0.1, 0.7, 0.8, 0.3, 0.2, 0.8, 0.1, 0.7]
Beta2 = [0.5, 0.6, 0.7, 0.9, 0.9, 0.8, 0.9, 0.8, 0.6, 0.5, 0.7, 0.6, 0.7, 0.7, 0.8, 0.8, 0.9, 0.8, 0.5, 0.7]

Beta1 = np.array(Beta1)
Beta2 = np.array(Beta2)

print(len(Beta1))
print(len(Beta2))


def cal_true_y_vector(X, Beta, epsilon): #epsilon should be the same on each iteration
    tmp = X.dot(Beta)
    res = tmp + epsilon
    return res;

def cal_pred_y_vector(X, y, lamda):
    X_T = X.T
    a = X_T.dot(X)
    b = lamda * np.eye(len(a))
    
    tmp = a + b
    tmp = np.linalg.inv(tmp)
    tmp = tmp.dot(X_T)
    tmp = tmp.dot(y)
    
    return X.dot(tmp);


def MSE(X, Beta1, Beta2, N):
    
    lamdas = np.linspace(0, 5, 200)
    
    beta1_mse = []
    beta2_mse = []
    
    for lamda in lamdas:
        tmp1 = []
        tmp2 = []
        for step in range(0, N):
            epslist = np.random.normal(0, 1, 50)
            #true value
            y_1 =  cal_true_y_vector(X, Beta1, epslist)
            y_2 =  cal_true_y_vector(X, Beta2, epslist)
            #pred value
            y_1_pred = cal_pred_y_vector(X, y_1, lamda)
            y_2_pred = cal_pred_y_vector(X, y_2, lamda)
            #calculate the error
            res1 = y_1 - y_1_pred
            res1 = res1 * res1
            res2 = y_2 - y_2_pred
            res2 = res2 * res2
            #put into tmp1 and tmp2
            tmp1.append(res1.sum())
            tmp2.append(res2.sum())
        #Get MSE for each lamda
        beta1_mse.append(sum(tmp1) / N)
        beta2_mse.append(sum(tmp2) / N)
            
    plt.figure()
    label = ["Beta1", "Beta2"]
    plt.plot(lamdas, beta1_mse,':')
    plt.plot(lamdas, beta2_mse,':')
    plt.xlabel('Lamda')
    plt.ylabel('MSE')
    plt.title('MSE Graph')
    plt.legend(label)
    plt.show()
    
    linear_mse_beta1 = beta1_mse[0]
    linear_mse_beta2 = beta2_mse[0]
    print("Linear MSE Comparsion Between Beta1 and Beta2")
    print("Beta1 MSE : %s" %linear_mse_beta1)
    print("Beta2 MSE : %s" %linear_mse_beta2)
    

###############Testing Area####################
MSE(X, Beta1, Beta2, 100)