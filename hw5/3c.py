#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 20:53:30 2018

@author: siyangchen
"""

import pandas as pd
import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt


###############Preprocessing Area#################

##Read X
np.random.seed(0)
df = pd.read_csv('X.csv')
X = df.as_matrix()
X = preprocessing.scale(X)

##SVD
Up, L, V_t = np.linalg.svd(X, full_matrices = False)
#print(len(Up))
#print(L)
#print(len(V_t[0]))

##Other Variable
lamda = 1
alpha = 0.1
beta = np.array([0.1, 0.3, 0.2, 0.2, 0.9, 0.8, 0.9, 0.1, 0.4, 0.2, 0.7, 0.3, 0.1, 0.7, 0.8, 0.3, 0.2, 0.8, 0.1, 0.7])

##Other Settings
np.random.seed(0)

##################################################
##################################################


def plot1(X, Up, L, V_t, lamda, alpha, beta, N):
    L = np.diag(L)
    
    ##True gamma
    gamma = V_t.dot(beta)
    
    ##Expectation of estimate gamma
    gamma_est = np.zeros(len(L))
    for i in range(N):
        eps = np.random.normal(0, 1, len(X))
        Y = np.ones(len(X)) * alpha + X.dot(beta) + eps
        
        a = L.dot(L)
        size = len(a)
        b = lamda * np.eye(size)
        
        tmp = a + b
        tmp = np.linalg.inv(tmp)
        tmp = tmp.dot(L)
        tmp = tmp.dot(Up.T)
        tmp = tmp.dot(Y)
        gamma_est = gamma_est + tmp
    
    gamma_est = gamma_est / N
    
    
    
    plt.figure()
    label = ["true_gamma", "expectation_est_gamma"]
    plt.plot(gamma,':')
    plt.plot(gamma_est,':')
    plt.xlabel('features')
    plt.ylabel('value')
    plt.title('N = %d times' %N)
    plt.legend(label)
    plt.show()
    


def plot2(X, Up, L, V_t, lamda, alpha, beta, N):
    L_diag = np.diag(L)
    #list1, list2
    list1 = []
    
    ##True gamma
    gamma = V_t.dot(beta)
    
    ##Expectation of estimate gamma
    list1 = np.zeros(len(L_diag))
    for i in range(N):
        eps = np.random.normal(0, 1, len(X))
        Y = np.ones(len(X)) * alpha + X.dot(beta) + eps
        
        a = L_diag.dot(L_diag)
        size = len(a)
        b = lamda * np.eye(size)
        
        tmp = a + b
        tmp = np.linalg.inv(tmp)
        tmp = tmp.dot(L_diag)
        tmp = tmp.dot(Up.T)
        est_gamma = tmp.dot(Y)
        
        tmp = est_gamma - gamma
        list1 = list1 + tmp * tmp
    
    list1 = list1 / N
    
    ##list2
    list2 = L * L + gamma * gamma
    temp = L * L + lamda * np.ones(len(L))
    temp = temp * temp
    list2 = list2 / temp
    
    
    plt.figure()
    label = ["techinique1", "techinique2"]
    plt.plot(list1,':')
    plt.plot(list2,':')
    plt.xlabel('features')
    plt.ylabel('value')
    plt.title('N = %d times' %N)
    plt.legend(label)
    plt.show()
    
    
    return 0;
    
########################Testing Area####################
plot1(X, Up, L, V_t, lamda, alpha, beta, 100)
plot2(X, Up, L, V_t, lamda, alpha, beta, 100)