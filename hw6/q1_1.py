#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 15:27:54 2018

@author: siyangchen
"""

########For Quesion 1.1#############

##One Layer NN can work
##Layer : 1
##Node : 2

import numpy as np
import matplotlib.pyplot as plt


def sigmoid(x):
  return 1 / (1 + np.exp(-x))


####
  ## w_input_to_layer, bias, w2_layer_to_output should all be two dimensions
####
def bump_creator(input_x, w_input_to_layer, bias, w_layer_to_output):
  node0_input = input_x * w_input_to_layer[0] + bias[0]
  node1_input = input_x * w_input_to_layer[1] + bias[1]
  
  node0_output = sigmoid(node0_input)
  node1_output = sigmoid(node1_input)

  y_res = node0_output * w_layer_to_output[0] + node1_output * w_layer_to_output[1]
  
  
  ##Comparing to the 1d bump, which is not the approximate one##
  x_bump = [0, 0.4, 0.4, 0.6, 0.6, 1]
  y_bump = [0, 0, 1, 1, 0, 0]
  
  
  plt.figure()
  label = ["Approximation Function", "1D Bump"]
  plt.plot(input_x, y_res, ':')
  plt.plot(x_bump, y_bump, ':')
  plt.xlabel('Input')
  plt.ylabel('Output')
  plt.title('My bump approximation')
  plt.legend(label)
  plt.show()
  
  return y_res



###################################
#######Testing Area################
##Sample Points
input_x = np.linspace(0, 1, 100)

##Parameters for creating the bump function
w_input_to_layer = [1000, 1000]
bias = [-400, -600]
w_layer_to_output = [1, -1]

##Execute the bump_creator function
bump_creator(input_x, w_input_to_layer, bias, w_layer_to_output)

