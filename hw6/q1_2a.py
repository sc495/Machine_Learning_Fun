#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 17:12:30 2018

@author: siyangchen
"""

########For Quesion 1.2.a#############

##One Layer NN can work
##Input : 2
##Layer : 1
##Node : 2

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def sigmoid(x):
  return 1 / (1 + np.exp(-x))


####
  ## w_input_to_layer, bias, w2_layer_to_output should all be two dimensions
####

def bump_creator(input_x1, input_x2, w_1_to_layer, w_2_to_layer, bias_1_to_layer, bias_2_to_layer, w_layer_to_output):
  
  [input_x1,input_x2] = np.meshgrid(input_x1,input_x2)
    
  node0_input = (input_x1 * w_1_to_layer[0] + bias_1_to_layer[0]) + (input_x2 * w_2_to_layer[0] + bias_2_to_layer[0])
  node1_input = (input_x1 * w_1_to_layer[1] + bias_1_to_layer[1]) + (input_x2 * w_2_to_layer[1] + bias_2_to_layer[1])
  
  node0_output = sigmoid(node0_input)
  node1_output = sigmoid(node1_input)

  y_res = node0_output * w_layer_to_output[0] + node1_output * w_layer_to_output[1]
  
  fig = plt.figure()
  ax = Axes3D(fig)


  print(input_x1)

  ax.plot_surface(input_x2, input_x1, y_res)

  plt.xlabel('x2')
  plt.ylabel('x1')
  plt.title('My Approximation')
  
  plt.show()
 
  
  return y_res



###################################
#######Testing Area################
##Sample Points
input_x1 = np.linspace(0, 1, 100)
input_x2 = np.linspace(0, 1, 100)

##Parameters for creating the bump function
w_1_to_layer = [1000, 1000]
w_2_to_layer = [0, 0]
bias_1_to_layer = [-300, -700]
bias_2_to_layer = [0, 0]
w_layer_to_output = [1, -1]

##Execute the bump_creator function
bump_creator(input_x1, input_x2, w_1_to_layer, w_2_to_layer, bias_1_to_layer, bias_2_to_layer, w_layer_to_output)

