#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  1 20:17:19 2018

@author: siyangchen
"""

########For Quesion 1.2.b#############

##One Layer NN can work
##Input : 2
##Layer : 2
##Node : 5 --- 4 nodes in first hidden layer and 1 node in second hidden layer


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def sigmoid(x):
  return 1 / (1 + np.exp(-x))



def bump_creator(input_x1, input_x2, w_1_to_layer1, w_2_to_layer1, bias_1_to_layer1, bias_2_to_layer1, w_l1_to_l2):
  
  [input_x1,input_x2] = np.meshgrid(input_x1,input_x2)
    
  node0_input = (input_x1 * w_1_to_layer1[0] + bias_1_to_layer1[0]) + (input_x2 * w_2_to_layer1[0] + bias_2_to_layer1[0])
  node1_input = (input_x1 * w_1_to_layer1[1] + bias_1_to_layer1[1]) + (input_x2 * w_2_to_layer1[1] + bias_2_to_layer1[1])
  
  node0_input = input_x1 * w_1_to_layer1[0] + bias_1_to_layer1[0]
  node1_input = input_x1 * w_1_to_layer1[1] + bias_1_to_layer1[1]
  node2_input = input_x2 * w_2_to_layer1[0] + bias_2_to_layer1[0]
  node3_input = input_x2 * w_2_to_layer1[1] + bias_2_to_layer1[1]
  
  
  node0_output = sigmoid(node0_input)
  node1_output = sigmoid(node1_input)
  node2_output = sigmoid(node2_input)
  node3_output = sigmoid(node3_input)



  y_res = node0_output * w_l1_to_l2[0] + \
          node1_output * w_l1_to_l2[1] + \
          node2_output * w_l1_to_l2[2] + \
          node3_output * w_l1_to_l2[3]
  
  y_res = y_res + bias_l1_to_l2
  y_res = sigmoid(y_res)
  
  fig = plt.figure()
  ax = Axes3D(fig)


  print(input_x1)

  ax.plot_surface(input_x2, input_x1, y_res)

  plt.xlabel('x2')
  plt.ylabel('x1')
  plt.title('My Approximation')
  
  plt.show()
 
  
  return y_res



###################################
#######Testing Area################
##Sample Points
input_x1 = np.linspace(0, 1, 100)
input_x2 = np.linspace(0, 1, 100)

##Parameters for creating the bump function
##First Hidden Layer
w_1_to_layer1 = [1000, 1000]
w_2_to_layer1 = [1000, 1000]
bias_1_to_layer1 = [-400, -600]
bias_2_to_layer1 = [-400, -600]
##Second Hidden Layer
w_l1_to_l2 = [1000, -1000, 1000, -1000]
bias_l1_to_l2 = -1500



##Execute the bump_creator function
bump_creator(input_x1, input_x2, w_1_to_layer1, w_2_to_layer1, bias_1_to_layer1, bias_2_to_layer1, w_l1_to_l2)




