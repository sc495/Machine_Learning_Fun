#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 15:14:39 2018

@author: siyangchen
"""

########For Quesion 2#############
######### EM Algorithm ###########
import random
from scipy.special import comb
import numpy as np

####Parameters####
n = 100 #Experimenting times in a single day
m = 6 #How many days for testing
theta1 = 0.80 #Machine 1 broken probability
theta2 = 0.30 #Machine 2 broken probability

######################
####Initialization####
est1 = 0.53 
est2 = 0.48

######################
######################


######################
###Helper Function###
def countBrokenTimes(times, machine1Chosen, theta1, theta2):
    tmpList = []
    if machine1Chosen == True:
        for i in range(times):
            tmp = random.random()
            if tmp < theta1:
                tmpList.append(1)
            else:
                tmpList.append(0)
    else:
        for i in range(times):
            tmp = random.random()
            if tmp < theta2:
                tmpList.append(1)
            else:
                tmpList.append(0)
    return tmpList.count(1)

######################
######################    

######################
####Main Function####
def generate_observation(m, n, theta1, theta2): #Assume only two machines, and equally random chosen
    tmpList = []
    for day in range(m):
        seed = random.randint(0,10000)
        if seed % 2 == 0: #Machine 1 is chosen
            tmpList.append(countBrokenTimes(n, True, theta1, theta2))
        else: #Machine 2 is chosen
            tmpList.append(countBrokenTimes(n, False, theta1, theta2))
            
    print("Observation is as following:")
    print(tmpList)
    return tmpList

def EM_estimation(m, n, theta1, theta2, est1, est2):
    
    observation = generate_observation(m, n, theta1, theta2)    
    obs_np = np.array(observation)
    
    iteration = 0
    prev_est1 = est1
    prev_est2 = est2
    
    while (1):
        
        #E-Step:
        Q1 = []
        Q2 = []
        for i in range(len(observation)):
            p1 = comb(n, observation[i]) * pow(est1, observation[i]) * pow(1 - est1, n - observation[i])
            p2 = comb(n, observation[i]) * pow(est2, observation[i]) * pow(1 - est2, n - observation[i])
            val1 = p1 / (p1 + p2)
            val2 = p2 / (p1 + p2)
            Q1.append(val1)
            Q2.append(val2)
        Q1 = np.array(Q1)
        Q2 = np.array(Q2)
    
        #M - Step
        QS1 = Q1 * obs_np
        QN1 = Q1 * n
        est1 = QS1.sum() / QN1.sum()
        QS2 = Q2 * obs_np
        QN2 = Q2 * n
        est2 = QS2.sum() / QN2.sum()
    
        #Result at this Iteration
        print("Iteration : %s" %iteration)
        print("Est_Beta1 : %s" %est1)
        print("Est_Beta2 : %s" %est2)
        
        if prev_est1 == est1 and prev_est2 == est2:
            break
        
        iteration += 1
        prev_est1 = est1
        prev_est2 = est2
        
    return

######################
######################

####Testing Area####
EM_estimation(m, n, theta1, theta2, est1, est2)
