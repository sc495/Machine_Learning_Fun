#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 13:36:49 2018

@author: siyangchen
"""

import pandas as pd
import heapq as pq
import matplotlib.pyplot as plt

############################
#######Preprocessing########
df_x = pd.read_csv('data.csv')

############################
######Helper Function#######
def cal_distance(list1, list2):
    assert(len(list1) == len(list2))
    dist = 0
    for i in range(len(list1) - 1):
        dist += pow(list1[i] - list2[i], 2)
    return dist

def find(df_x, x):
    if df_x.loc[x]['Belong'] == x:
        return x
    df_x.set_value(x, 'Belong', find(df_x, df_x.loc[x]['Belong']))
    return df_x.loc[x]['Belong']


def plot2D(df_x, clusters): #Only 2-dimension data
    plt.figure()
    for key in clusters:
        df_tmp = df_x[df_x['Belong'] == key]
        p_x0 = df_tmp['0'].tolist()
        p_x1 = df_tmp['1'].tolist()
        plt.scatter(p_x0, p_x1)
        
    plt.show()

############################
#######Main Function########
def agglomerative(df_x, k):

    df_x['Belong'] = df_x.index    
    
    #priority_queue --- sorting the distance between points
    heap = []
    for i in range(1, len(df_x)):
        for j in range(i):
            pq.heappush(heap, (cal_distance(df_x.loc[i].tolist(), df_x.loc[j].tolist()), j, i))

    #Union find --- find and connect points
    while len(set(df_x['Belong'].tolist())) > k:
        tmp = pq.heappop(heap)
        root_a = find(df_x, tmp[1])
        root_b = find(df_x, tmp[2])
        if root_a != root_b:
            df_x.set_value(root_a, 'Belong', root_b)
    
    plot2D(df_x, set(df_x['Belong'].tolist())) #Only Two Dimensions can work
    
    return df_x


############################
#######Testing Area#########
agglomerative(df_x, 2)