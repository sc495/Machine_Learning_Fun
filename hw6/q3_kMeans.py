# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 23:01:13 2018

@author: siyang
"""
import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt 


########For Quesion 3 - K Means#############
###########"K Means"##############


######Preprocessing######
df_x = pd.read_csv('data.csv')

######Helper Function######
def plot2D(df_x, clusters): #Only 2-dimension data

    df_0 = df_x[df_x['Belong'] == 0]
    p0_x0 = df_0['0'].tolist()
    p0_x1 = df_0['1'].tolist()
    df_1 = df_x[df_x['Belong'] == 1]
    p1_x0 = df_1['0'].tolist()
    p1_x1 = df_1['1'].tolist()
    
    plt.figure()
    plt.scatter(clusters[0][0], clusters[0][1], s = 1000, c='r', marker='+')
    plt.scatter(p0_x0, p0_x1, c='r')
    plt.scatter(clusters[1][0], clusters[1][1], s = 1000, c='b', marker='+')
    plt.scatter(p1_x0, p1_x1, c='b')

    plt.show()

#####Main Function#####
def kMeans(df_x, k):
    
    dimension = len(df_x.loc[0])
    clusters = df_x.sample(k)
    clusters = clusters.as_matrix()
    print(dimension)
    print(clusters)
    
    df_x['Belong'] = 0

    #K-Means until Convergence : Assume Convergen can be achieved
    while(1):
        #step1: find the closest cluster for each point
        for i in range(0, len(df_x)):
            sumTmp = []
            for c in range(k):
                tmp = 0
                for d in range(dimension):
                    tmp += pow(df_x.loc[i][d] - clusters[c][d], 2)
                sumTmp.append(tmp)
            df_x.set_value(i, 'Belong', sumTmp.index(min(sumTmp)))
                    
        #step2: Assign new cluster
        prev_clusters = copy.deepcopy(clusters)
        for c in range(k):
            if len(df_x[df_x['Belong'] == c]) != 0:
                df_tmp = df_x[df_x['Belong'] == c].sum().tolist()
                for d in range(dimension):
                    clusters[c][d] = df_tmp[d] / len(df_x[df_x['Belong'] == c])
        
        #step3: when it comes to convergence
        if np.array_equal(prev_clusters, clusters):
            break

    

    print("***********************")    
    print("yeah, it works")
    print(df_x)
    #print(df_x[df_x['Belong'] == 0])
    #print(df_x[df_x['Belong'] == 1])
    print(clusters[0])
    print(clusters[1])
    plot2D(df_x, clusters)
    
    return [df_x, clusters] #df_x['belong'] can know each point belonged to which cluster

#####Testing Area#####
kMeans(df_x, 2)
